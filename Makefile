CC = gcc
CFLAGS = -O0 -g -std=c18 -march=native -fstack-protector-all -c -pedantic -Wextra
LDLIBS = -lpulse -lm



OBJECTS = $(subst src/,objs/,\
	$(subst .c,.o,\
	$(shell ls src/*.c)))



all: bin/sound-generator

bin/sound-generator: $(OBJECTS)
	@mkdir -p bin
	$(CC) $^ $(LDLIBS) -o $@

$(OBJECTS):
	@mkdir -p objs
	$(CC) $(CFLAGS) src/$(notdir $(subst .o,.c,$(@))) -o $(@)



.PHONY: clean
clean:
	rm -rf bin objs
