#include <pulse/def.h>
#include <pulse/channelmap.h>
#include <pulse/context.h>
#include <pulse/error.h>
#include <pulse/sample.h>
#include <pulse/stream.h>
#include <pulse/thread-mainloop.h>


#include <errno.h>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>


#include "main.h"
#include "pa-stream-write-callback.h" // Defines pa_stream_write_callback(), passed to pa_stream_set_write_callback();

struct globals globals;


int main(){
	// Declare a couple of structs for configuring PA
	static const pa_sample_spec sample_spec = {
		.format = PA_SAMPLE_S32BE,
		.rate = 192000,
		.channels = 2
	};

	static const pa_channel_map channel_map = {
		.channels = 2,
		.map = {
			PA_CHANNEL_POSITION_FRONT_LEFT,
			PA_CHANNEL_POSITION_FRONT_RIGHT
		}
	};

	// Initialize globals struct
	globals.pa_mainloop_started = 0;
	globals.pa_mainloop_locked = 0;
	globals.pa_context_started = 0;
	globals.pa_stream_started = 0;
	globals.application_ready = 0;
	globals.application_exit = 0;
	globals.return_status = 0;
	globals.perror_string = NULL;

	globals.pa_threaded_mainloop = NULL;
	globals.pa_threaded_mainloop_api = NULL;
	globals.pa_context = NULL;
	globals.pa_stream = NULL;
	globals.sample_bytes = pa_sample_size(&sample_spec);

	// Check configurations
	if(globals.sample_bytes < 1 || globals.sample_bytes > 4){
		globals.return_status = 1;
		globals.perror_string = "Sample bytes not between 1 and 4?";
		goto APPLICATION_END;
	}

	if(!pa_channel_map_valid(&channel_map)){
		globals.return_status = 1;
		globals.perror_string = "Invalid channel map";
		goto APPLICATION_END;
	}

	if(!pa_sample_spec_valid(&sample_spec)){
		globals.return_status = 1;
		globals.perror_string = "Invalid sample spec";
		goto APPLICATION_END;
	}


	// Set up PA threaded mainloop (Asynchronous API)
	globals.pa_threaded_mainloop = pa_threaded_mainloop_new();
	globals.pa_threaded_mainloop_api = pa_threaded_mainloop_get_api(globals.pa_threaded_mainloop);
	globals.pa_mainloop_started = 1;

	if(pa_threaded_mainloop_start(globals.pa_threaded_mainloop) < 0){
		globals.return_status = 1;
		globals.perror_string = "pa_mainloop_start() failed";
		goto APPLICATION_END;
	}


	// Set up PA context
	globals.pa_context = pa_context_new(globals.pa_threaded_mainloop_api, "Audio Generator");
	if(globals.pa_context == NULL){
		globals.return_status = 1;
		globals.perror_string = "pa_context_new() failed";
		goto APPLICATION_END;
	}

	globals.pa_context_started = 1;

	globals.pa_mainloop_locked = 1;
	pa_threaded_mainloop_lock(globals.pa_threaded_mainloop);
    if(pa_context_connect(globals.pa_context, NULL, 0, NULL) < 0) {
        fprintf(stderr, "pa_context_connect() failed: %s\n", pa_strerror(pa_context_errno(globals.pa_context)));
		globals.return_status = 1;
		globals.perror_string = "";
		goto APPLICATION_END;
    }	
	pa_threaded_mainloop_unlock(globals.pa_threaded_mainloop);
	globals.pa_mainloop_locked = 0;

	// Spin lock until the PA server is done setting up the context
	PA_CONTEXT_READY_WAIT:
	globals.pa_mainloop_locked = 1;
	pa_threaded_mainloop_lock(globals.pa_threaded_mainloop);

	switch(pa_context_get_state(globals.pa_context)){
		case PA_CONTEXT_READY:
			pa_threaded_mainloop_unlock(globals.pa_threaded_mainloop);
			globals.pa_mainloop_locked = 0;
		break;

		case PA_CONTEXT_FAILED:
			fprintf(stderr, "pa_context_connect() failed (later): %s\n", pa_strerror(pa_context_errno(globals.pa_context)));
			globals.return_status = 1;
			globals.perror_string = "";
		goto APPLICATION_END;
		

		case PA_CONTEXT_TERMINATED:
			globals.return_status = 1;
			globals.perror_string = "pa_context_connnect failed (terminated?)";
		goto APPLICATION_END;

		default:
			pa_threaded_mainloop_unlock(globals.pa_threaded_mainloop);
			globals.pa_mainloop_locked = 0;
		goto PA_CONTEXT_READY_WAIT;
	}


	// Set up PA stream
	globals.pa_mainloop_locked = 1;
	pa_threaded_mainloop_lock(globals.pa_threaded_mainloop);
	globals.pa_stream = pa_stream_new(globals.pa_context, "Sound Generator", &sample_spec, &channel_map);
	pa_threaded_mainloop_unlock(globals.pa_threaded_mainloop);
	globals.pa_mainloop_locked = 0;

	if(globals.pa_stream == NULL){
		fprintf(stderr, "pa_stream_new() failed: %s\n", pa_strerror(pa_context_errno(globals.pa_context)));
		globals.return_status = 1;
		globals.perror_string = "";
		goto APPLICATION_END;
	}

	globals.pa_stream_started = 1;


	globals.pa_mainloop_locked = 1;
	pa_threaded_mainloop_lock(globals.pa_threaded_mainloop);
	pa_stream_set_write_callback(globals.pa_stream, pa_stream_write_callback, NULL);
	pa_threaded_mainloop_unlock(globals.pa_threaded_mainloop);
	globals.pa_mainloop_locked = 0;


	globals.pa_mainloop_locked = 1;
	pa_threaded_mainloop_lock(globals.pa_threaded_mainloop);

	if(pa_stream_connect_playback(globals.pa_stream, NULL, NULL, 0, NULL, NULL)){
		fprintf(stderr, "pa_stream_connect_playback() failed: %s\n", pa_strerror(pa_context_errno(globals.pa_context)));
		globals.return_status = 1;
		globals.perror_string = "";
		goto APPLICATION_END;
	}

	pa_threaded_mainloop_unlock(globals.pa_threaded_mainloop);
	globals.pa_mainloop_locked = 0;


	// PA set up is done, application may now start
	globals.application_ready = 1;

	printf("Application ready!\n");

	while(!globals.application_exit)
		sleep(1);


APPLICATION_END:
	// Cleanup
	if(globals.perror_string != NULL)
		perror(globals.perror_string);

	if(globals.pa_mainloop_locked)
		pa_threaded_mainloop_unlock(globals.pa_threaded_mainloop);

	if(globals.pa_stream_started){
		pa_stream_disconnect(globals.pa_stream);
		pa_stream_unref(globals.pa_stream);
	}

	if(globals.pa_context_started){
		pa_context_disconnect(globals.pa_context);
		pa_context_unref(globals.pa_context);
	}

	if(globals.pa_mainloop_started){
		pa_threaded_mainloop_stop(globals.pa_threaded_mainloop);
		pa_threaded_mainloop_free(globals.pa_threaded_mainloop);
	}

	return globals.return_status;
}
