struct globals {
	uint8_t pa_mainloop_started:1;
	uint8_t pa_mainloop_locked:1;
	uint8_t pa_context_started:1;
	uint8_t pa_stream_started:1;
	uint8_t application_ready:1;
	uint8_t application_exit:1;
	uint8_t return_status;
	char *perror_string;

	pa_threaded_mainloop *pa_threaded_mainloop;
	pa_mainloop_api *pa_threaded_mainloop_api;
	pa_context *pa_context;
	pa_stream *pa_stream;
	size_t sample_bytes;
};
