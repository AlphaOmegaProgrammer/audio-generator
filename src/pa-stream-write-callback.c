#include "main.h"

void pa_stream_write_callback(pa_stream *audio_stream, size_t nbytes, void* userdata){
	if(globals.application_exit || !globals.application_ready)
		return;

	printf("stream write callback called\n");

	static uint64_t i;
	static uint8_t *buffer;
	static size_t buffer_size;
	static uint64_t buffer_wave_index = 0;

	buffer = NULL;
	buffer_size = -1;

	if(pa_stream_begin_write(audio_stream, (void**)&buffer, &buffer_size) != 0 || buffer == NULL || buffer_size < 1 || (buffer_size % globals.sample_bytes) != 0){
		fprintf(stderr, "pa_stream_begin_write() failed: %s\n", pa_strerror(pa_context_errno(globals.pa_context)));
		globals.return_status = 1;
		globals.perror_string = "";
		globals.application_exit = 1;
		return;
	}

	printf("buffer size: %zi\n", buffer_size);

	for(i=0; i+globals.sample_bytes < buffer_size; i+=globals.buffer_size){
		if(buffer_wave_index < 1000){
			buffer_wave_index++;
			buffer[i] = 0x10;
			buffer[i+1] = 0x00;
			buffer[i+2] = 0x00;
			buffer[i+3] = 0x10;
			buffer[i+4] = 0x00;
			buffer[i+5] = 0x00;
		}else{
			buffer_wave_index++;
			buffer[i] = 0x00;
			buffer[i+1] = 0x00;
			buffer[i+2] = 0x00;
			buffer[i+3] = 0x00;
			buffer[i+4] = 0x00;
			buffer[i+5] = 0x00;
			if(buffer_wave_index >= 2000)
				buffer_wave_index = 0;
		}
	}

	if(pa_stream_write(audio_stream, buffer, buffer_size, NULL, 0, PA_SEEK_RELATIVE) != 0){
		fprintf(stderr, "pa_stream_write() failed: %s\n", pa_strerror(pa_context_errno(globals.pa_context)));
		globals.return_status = 1;
		globals.perror_string = "";
		globals.application_exit = 1;
		return;
	}


	printf("buffer wrote\n");

/*
	char *crash = 0;
	crash[0] = 25;
	crash[1] = 25;
	crash[2] = 25;
*/
}
