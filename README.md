**WARNING** Be very careful of volume and waves when generating sound. You can cause **permament** damage to things.

This was my attempt to create an audio generator that played back audio to pulseaudio. If I remember right, I got it to work somewhat, but I also wanted to save the playback to a `.flac` file in real time, and at the time my C wasn't good enough to figure out how to do this efficiently.

I definitely want to return to this at some point in the future, but I'm not sure when I'll be able to.

Also, look in the Brief.txt file in the docs folder for some notes I took on how sound works at the programatic level.
